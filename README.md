# pexl-C

![](https://img.shields.io/badge/written%20in-C%2B%2B-blue)

An embeddable programming language.

A single .cpp/.h pair defines this programming language, that can be embedded into a C++ program and call back to C++ functions. It uses an LL(0) parser and walks the generated AST. 

The language has been totally rewritten a number of times since, this project entry represents the "-C" snapshot.

Source code, binary, and factorial sample included in archive.

Tags: PL


## Download

- [⬇️ pexl-0.1.7z](dist-archive/pexl-0.1.7z) *(21.23 KiB)*
